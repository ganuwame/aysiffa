import {
  Button,
  Card,
  Image,
  Stack,
  CardBody,
  CardFooter,
  Heading,
  Text,
  Flex,
  Spacer,
  IconButton
} from '@chakra-ui/react';
import React from 'react';
import BaseLayout from '../../components/BaseLayout';
import a2 from '../../assets/images/a2.jpg';
import f1 from '../../assets/images/f1.jpg';
import d1 from '../../assets/images/d1.jpg';
import b1 from '../../assets/images/b1.jpg';
import m1 from '../../assets/images/m1.jpg';
import { useNavigate } from 'react-router-dom';
import { FaWhatsapp, FaHome } from 'react-icons/fa';

const waLink = (product) => {
  window.location.href = `https://api.whatsapp.com/send?phone=6281261259602&text=Halo%20kak%20Siffa%2C%20mau%20order%20${product}%20dong..`;
};

export default function CakeShop() {
  const navigate = useNavigate();
  return (
    <BaseLayout>
      <Flex
        justifyContent="start"
        alignItems="start"
        flexDirection="row"
        mb={5}
      >
        <Heading>
          <Text
            fontWeight="Bold"
            fontFamily="Caramel"
            fontSize="70px"
            color="red.400"
          >
            Aysiffa Shop
          </Text>
          <Text
            fontWeight="400"
            color="red.400"
          >Cake & Donnut</Text>
        </Heading>
        <Spacer />
        <IconButton
          bg="red.200"
          aria-label="Home"
          icon={<FaHome />}
          onClick={()=>navigate('/')}
          size="sm"
        />
      </Flex>

      <Card
        mb={10}
        direction={{ base: 'column', sm: 'row' }}
        overflow='hidden'
        variant='outline'
      >
        <Image
          objectFit='cover'
          maxW={{ base: '100%', sm: '200px' }}
          src={a2}
          alt='Donnut Romantis'
        />

        <Stack>
          <CardBody>
            <Heading size='md'>Donnut Romantis</Heading>

            <Text pt='2'>
              Yuk Order!
            </Text>
            <Text py='2'>
              Donnut Romantis cocok untuk kado ulang tahun, acara spesial, dan berbagi kebahagiaan lainnya.
            </Text>
            <Text>
              Label bisa request dan kustom sesuai keinginan ya.
            </Text>
            <Text pt='2'>
              Satu paket berisi 12 pcs dengan aneka varian topping yang akan memanjakan mata dan membuncah rasa di dada ❤️😍
            </Text>
          </CardBody>

          <CardFooter>
            <Button w="100%" variant='solid' colorScheme='green' onClick={() => waLink('Donnut Romantis')}>
              <FaWhatsapp />
              <Text ml="5px">Pesan Sekarang</Text>
            </Button>
          </CardFooter>
        </Stack>
      </Card>

      <Card
        mb={10}
        direction={{ base: 'column', sm: 'row' }}
        overflow='hidden'
        variant='outline'
      >
        <Image
          objectFit='cover'
          maxW={{ base: '100%', sm: '200px' }}
          src={f1}
          alt='Brownies Fudgy'
        />

        <Stack>
          <CardBody>
            <Heading size='md'>Brownies Fudgy</Heading>

            <Text pt='2'>
              Yuk order!
            </Text>
            <Text pt='2'>
              Brownies Fudgy lembut dengan aneka topping rasa yang siap membuat lidah kamu menari bergembira.
            </Text>
            <Text pt='2'>
              Cocok untuk menyertai kebersamaan kamu apalagi dengan si dia.. dia.. dia.. dan dia.. teman-teman semua 😁
            </Text>
            <Text pt='2'>
              Bisa request topping sesuai selera,
            </Text>
            <Text
              fontWeight="700"
              pt='2'
            >
              Pilihan topping:
            </Text>
            <Text>
              Hazelnut
              - Oreo
              - Strawberry
              - Keju
              - Kacang
              - Choco Chips
            </Text>
          </CardBody>

          <CardFooter>
            <Button w="100%" variant='solid' colorScheme='green' onClick={() => waLink('Brownies Fudgy')}>
              <FaWhatsapp />
              <Text ml="5px">Pesan Sekarang</Text>
            </Button>
          </CardFooter>
        </Stack>
      </Card>

      <Card
        mb={10}
        direction={{ base: 'column', sm: 'row' }}
        overflow='hidden'
        variant='outline'
      >
        <Image
          objectFit='cover'
          maxW={{ base: '100%', sm: '200px' }}
          src={d1}
          alt='Donnut Kampung'
        />

        <Stack>
          <CardBody>
            <Heading size='md'>Donnut Kampung</Heading>

            <Text pt='2'>
              Yuk order!
            </Text>
            <Text py='2'>
              Donnut Kampung, lembut dan enak tanpa pengawet. Bisa request topping sesuai pesanan.
            </Text>
            <Text py='2'>
              Cocok buat kamu yang rindu jajan masa belia 😋
            </Text>
          </CardBody>

          <CardFooter>
            <Button w="100%" variant='solid' colorScheme='green' onClick={() => waLink('Donnut Kampung')}>
              <FaWhatsapp />
              <Text ml="5px">Pesan Sekarang</Text>
            </Button>
          </CardFooter>
        </Stack>
      </Card>

      <Card
        mb={10}
        direction={{ base: 'column', sm: 'row' }}
        overflow='hidden'
        variant='outline'
      >
        <Image
          objectFit='cover'
          maxW={{ base: '100%', sm: '200px' }}
          src={b1}
          alt='Bolu Gembira'
        />

        <Stack>
          <CardBody>
            <Heading size='md'>Bolu Gembira</Heading>
            <Text pt='2'>
              Yuk order!
            </Text>
            <Text pt='2'>
              Bolu Gembira yang lembut tanpa pengawet dan pemanis buatan.
            </Text>
            <Text pt='2'>
              Dengan aneka topping rasa yang siap membuat lidah kamu berdansa gembira. 😄
            </Text>
          </CardBody>

          <CardFooter>
            <Button w="100%" variant='solid' colorScheme='green' onClick={() => waLink('Bolu Gembira')}>
              <FaWhatsapp />
              <Text ml="5px">Pesan Sekarang</Text>
            </Button>
          </CardFooter>
        </Stack>
      </Card>

      <Card
        mb={10}
        direction={{ base: 'column', sm: 'row' }}
        overflow='hidden'
        variant='outline'
      >
        <Image
          objectFit='cover'
          maxW={{ base: '100%', sm: '200px' }}
          src={m1}
          alt='Mie Goreng Ceriwis'
        />

        <Stack>
          <CardBody>
            <Heading size='md'>Mie Goreng Ceriwis</Heading>
            <Text pt='2'>
              Yuk order!
            </Text>
            <Text py='2'>
              Mie Goreng Spesial, tanpa penyedap dan pengawet. Bisa request level pedas juga ya.
            </Text>
            <Text py='2'>
              Enak, gurih, pedas, dan ngangenin.. 😉
            </Text>
          </CardBody>

          <CardFooter>
            <Button w="100%" variant='solid' colorScheme='green' onClick={() => waLink('Mie Goreng Ceriwis')}>
              <FaWhatsapp />
              <Text ml="5px">Pesan Sekarang</Text>
            </Button>
          </CardFooter>
        </Stack>
      </Card>
    </BaseLayout>
  )
}
