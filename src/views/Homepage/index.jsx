import { Container, Flex, Text, Image, Button } from '@chakra-ui/react';
import React from 'react';
import siffa from '../../assets/images/aysiffa.png';
import { useNavigate } from 'react-router-dom';
import { FaStore, FaBirthdayCake } from 'react-icons/fa';

export default function Homepage() {
  const navigate = useNavigate();
  const d = new Date();
  const today = `${d.getFullYear()}${(d.getMonth() + 1)}${d.getDate()}`;
  return (
    <Container
      px={{
        base: '0px'
      }}
    >
      <Flex
        h="100vh"
        bg="red.100"
        flexDirection="column"
        alignItems="center"
        justifyContent="center"
        gap={2}
      >
        <Image
          borderRadius='full'
          boxSize='150px'
          src={siffa}
        />
        <Text
          fontFamily="Caramel"
          fontSize="40px"
        >
          Nur Ain Aysiffa
        </Text>
        <Text
          fontFamily="Caramel"
          fontSize="40px"
        >
          (Cinderella)
        </Text>
        <Button
          width="70%"
          onClick={()=>navigate('/shop')}
        >
          <FaStore />
          <Text ml="5px">
            Aysiffa Online Shop
          </Text>
        </Button>
        {
          today === '20221222'? (
            <Button
              width="70%"
              onClick={()=>{ window.location.href='https://aysiffa.online/milad/'; }}
            >
            <FaBirthdayCake />
            <Text ml="5px">
              Today is My Birthday
            </Text>
            </Button>
          ) : ( '' )
        }
      </Flex>
    </Container>
  )
}
