import React from 'react';
import { Container } from '@chakra-ui/react';

function BaseLayout({ children }) {
  return (
    <Container
      py={{
        base: '4',
        md: '5',
        lg: '10'
      }}
    >
      {children}
    </Container>
  )
}

export default BaseLayout;