import React from "react";
import { ChakraProvider } from '@chakra-ui/react';
import { Routes, Route } from 'react-router-dom';

import Homepage from './views/Homepage';
import CakeShop from './views/CakeShop';

function App() {
  return (
    <ChakraProvider>
      <Routes>
        <Route path="/" element={<Homepage />} />
        <Route path="shop" element={<CakeShop />} />
      </Routes>
    </ChakraProvider>
  );
}

export default App;
